//
//  Task.Priority.swift
//  TaskList
//
//  Created by Femi Adegbite on 22/03/2020.
//  Copyright © 2020 Efhem. All rights reserved.
//

extension Task {
    
    enum Priority {
        case no, low, medium, high
    }
}
