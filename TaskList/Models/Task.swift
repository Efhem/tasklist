//
//  Task.swift
//  TaskList
//
//  Created by Femi Adegbite on 14/03/2020.
//  Copyright © 2020 Efhem. All rights reserved.
//
import Foundation

struct Task: Identifiable {
    
    let id = UUID() //optional parameter
    
    var name: String // required parameter
    var completed = false //optional parameter
}
