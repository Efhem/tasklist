//
//  TaskStore.swift
//  TaskList
//
//  Created by Femi Adegbite on 14/03/2020.
//  Copyright © 2020 Efhem. All rights reserved.
//

import Combine

class TaskStore: ObservableObject { //ObservableObject: to include taskStore on the observable
    
    @Published var tasks = [
        "Code a swift app",
        "Book an escape room",
        "Walk the cat",
        "Pick up heavy things and put them down",
        "Make karaoke playlist",
        "Present at iOS meetup group ",
        "Climb El captain",
        "Learn to make baklava",
        "Play dis golf in every estate",
        "100 movie marathon"
        ].map{Task(name:$0)}

    @Published var prioritizedTasks = [
        PrioritizedTasks(
            priority: .high, names: [
                "Code a swift app",
                "Book an escape room",
                "Walk the cat",
                "Pick up heavy things and put them down"
            ]
        ),
        PrioritizedTasks(
            priority: .medium, names: [
                "Make karaoke playlist",
                "Present at iOS meetup group "
            ]
        ),
        PrioritizedTasks(
            priority: .low, names: [
                "Climb El captain"
            ]
        ),
        PrioritizedTasks(
            priority: .no, names: [
                "Learn to make baklava",
                "Play dis golf in every estate",
                "100 movie marathon"
            ]
        )
    ]
}


extension TaskStore.PrioritizedTasks {
    init(priority: Task.Priority, names: [String]) {
        self.init(priority: priority, tasks: names.map{Task(name:$0)})
    }
}
