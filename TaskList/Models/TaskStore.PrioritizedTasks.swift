//
//  TaskStore.PrioritizedTasks.swift
//  TaskList
//
//  Created by Femi Adegbite on 22/03/2020.
//  Copyright © 2020 Efhem. All rights reserved.
//

extension TaskStore{
    struct PrioritizedTasks {
        let priority: Task.Priority
        var tasks : [Task]
    }
}

extension TaskStore.PrioritizedTasks: Identifiable {
    var id: Task.Priority { priority }
}
