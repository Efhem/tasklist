//
//  ContentView.swift
//  TaskList
//
//  Created by Femi Adegbite on 14/03/2020.
//  Copyright © 2020 Efhem. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject  var taskStore : TaskStore
    @State var modalPresented = false
    
    
    var body: some View {
        
        NavigationView {
            List {
                ForEach(
                    Array(  taskStore.tasks.enumerated()  ),
                         id: \.element.id
                
                ){ index, _ in
                    RowView(task: self.$taskStore.tasks[index])
                }
                .onMove { sourceIndices, destinatonIndex in
                    self.taskStore.tasks.move(fromOffsets: sourceIndices, toOffset: destinatonIndex)
                }
                .onDelete { indexSet in
                    self.taskStore.tasks.remove(atOffsets: indexSet)
                }
            }
            .navigationBarTitle("Tasks")
            .navigationBarItems(
                leading: EditButton(),
                trailing: Button(action: {self.modalPresented = true}) {
                    Image(systemName: "plus")
            })
        }
        .sheet(isPresented: $modalPresented) {
            NewTaskView(taskStore: self.taskStore)
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(taskStore: TaskStore())
    }
}


