//
//  RowView.swift
//  TaskList
//
//  Created by Femi Adegbite on 19/03/2020.
//  Copyright © 2020 Efhem. All rights reserved.
//

import SwiftUI

struct RowView: View {
    
    //Binding: it lets us declare that one value actually comes from elsewhere, and should be shared in both places
    @Binding var task: Task
    let checkmark = Image(systemName: "checkmark")
    
    var body: some View {
        NavigationLink(destination: TaskEditingView(task: self.$task)){
            
            if task.completed {
                checkmark
            }else{
                checkmark.hidden()
            }
            Text(task.name).strikethrough(task.completed)
        }
    }
}

struct RowView_Previews: PreviewProvider {
    static var previews: some View {
        RowView(task: .constant(Task(name: "Todo")))
    }
}
